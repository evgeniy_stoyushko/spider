from rest_framework import viewsets
from .serializers import DistrictSerializer
from .models import District


class DistrictsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ViewSet для отображения списка организаций в районе города
    """
    serializer_class = DistrictSerializer
    queryset = District.objects.all()