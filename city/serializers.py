from rest_framework import serializers
from city.models import District
from organization.serializers import OrganizationSimpleSerializer
from rest_framework.reverse import reverse


class DistrictSerializer(serializers.HyperlinkedModelSerializer):
    """
    Сериализатор отображения списка организаций в районе города
    """
    organizations = OrganizationSimpleSerializer(many=True, read_only=True)

    class Meta:
        model = District
        fields = ('id', 'url', 'name', 'organizations')


class DistrictOrganizationsURL(serializers.HyperlinkedIdentityField):
    """
    Поле для URL списка организаций в районе
    """
    def get_url(self, obj, view_name, request, format):
        return reverse(viewname=view_name, request=request, kwargs={'district_id': obj.id})


class DistrictOrganizationsSerializer(serializers.ModelSerializer):
    """
    Сериализатор списка районов
    """
    url = DistrictOrganizationsURL(read_only=True, view_name='organizations-detail')

    class Meta:
        model = District
        fields = ['id', 'url', 'name']
