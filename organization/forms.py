from django import forms
from dal import autocomplete
from .models import Catalog


class CatalogForm(forms.ModelForm):
    """
    Форма для заполнения каталога услуг с поиском товара
    """
    class Meta:
        model = Catalog
        fields = ('organization', 'product', 'price')
        widgets = {
            'product': autocomplete.ModelSelect2(url='product-autocomplete', forward=['organization']),
        }
