from rest_framework import serializers
from .models import Organization, Catalog, Concern
from product.serializers import ProductSimpleSerializer


class CatalogSerializer(serializers.HyperlinkedModelSerializer):
    """
    Сериализатор каталога услуг организации
    """
    product = ProductSimpleSerializer(read_only=True)

    class Meta:
        model = Catalog
        fields = ('price', 'product')


class ConcernSerializer(serializers.ModelSerializer):
    """
    Сериализатор списка организаций
    """
    class Meta:
        model = Concern
        fields = ('id', 'url', 'name')


class OrganizationSimpleSerializer(serializers.HyperlinkedModelSerializer):
    """
    Сериализатор для отображения списка организаций
    """
    concern = ConcernSerializer(read_only=True)

    class Meta:
        model = Organization
        fields = ('id', 'url', 'name', 'description', 'concern')


class OrganizationDetailSerializer(serializers.HyperlinkedModelSerializer):
    """
    Сериализатор для отображения деталей организации
    """
    concern = ConcernSerializer(read_only=True)
    catalog_products = CatalogSerializer(many=True, read_only=True)

    class Meta:
        model = Organization
        fields = ('id', 'url', 'name', 'description', 'concern', 'catalog_products')
