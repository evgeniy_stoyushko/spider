import django_filters
from dal import autocomplete
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.response import Response
from .models import Organization, Concern
from .serializers import OrganizationSimpleSerializer, ConcernSerializer, OrganizationDetailSerializer
from product.models import Product, Category
from city.models import District
from city.serializers import DistrictOrganizationsSerializer


class ProductAutocomplete(autocomplete.Select2QuerySetView):
    """
    Контролллер для поиска товара/услуги сети предприятия
    """
    def get_queryset(self):
        queryset = Product.objects.all().order_by('name')
        q = self.request.GET.get('q', None)
        organization = self.forwarded.get('organization', None)
        if organization:
            try:
                organization_instance = Organization.objects.get(pk=organization)
                queryset = queryset.filter(concern=organization_instance.concern)
            except Organization.DoesNotExist:
                pass
        if q:
            queryset = queryset.filter(name__icontains=q)
        return queryset


class ConcernViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ViewSet списка организаций
    """
    serializer_class = ConcernSerializer
    queryset = Concern.objects.all()


class OrganizationViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ViewSet организации
    """
    serializer_class = OrganizationSimpleSerializer
    queryset = Organization.objects.all()

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = OrganizationDetailSerializer
        return super(OrganizationViewSet, self).retrieve(request, *args, **kwargs)


class OrganizationFilter(django_filters.FilterSet):
    """
    Фильтр для списка организации
    """
    name = django_filters.CharFilter(name='name', lookup_expr='icontains', label='Название организации')
    product_name = django_filters.CharFilter(name='catalog_products__product__name', lookup_expr='icontains',
                                             label='Название товара')
    max_price = django_filters.NumberFilter(name='catalog_products__price', lookup_expr='lte',
                                            label='Максимальная цена')
    min_price = django_filters.NumberFilter(name='catalog_products__price', lookup_expr='gte', label='Минимальная цена')
    category = django_filters.ModelChoiceFilter(name='catalog_products__product__category',
                                                queryset=Category.objects.all(), to_field_name='id', label='Категория')

    class Meta:
        model = Organization
        fields = ['name', 'product_name', 'min_price', 'max_price']


class OrganizationsViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
    ViewSet списка организаций в районе города с фильтрацией по услугам
    """
    serializer_class = OrganizationSimpleSerializer
    lookup_field = 'district_id'
    filter_class = OrganizationFilter
    queryset = Organization.objects.all()

    def list(self, request, *args, **kwargs):
        """
        Список районов
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.filter_class = None
        queryset = District.objects.all()
        serializer = DistrictOrganizationsSerializer(queryset, many=True, context={'request':self.request})
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        """
        Список организаций в районе с фильтрацией по продуктам
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        queryset = self.get_queryset().filter(districts=kwargs.get('district_id'))
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
