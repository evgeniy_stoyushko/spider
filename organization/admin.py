from django.contrib import admin
from django.forms import Textarea
from django.db import models
from .models import Concern, Organization, Catalog
from .forms import CatalogForm


class OrganizationInline(admin.StackedInline):
    """
    Связные объекты для организаций в сети предприятий
    """
    model = Organization
    fields = (('name', 'concern'), ('description', 'districts'))
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 10, 'cols': 30})},
    }


class ConcernAdmin(admin.ModelAdmin):
    """
    Админка сети предприятий
    """
    model = Concern
    inlines = [OrganizationInline]


class CatalogInlineAdmin(admin.StackedInline):
    """
    Связные объекты каталога услуг для предприятия
    """
    model = Catalog
    form = CatalogForm
    fields = (('product', 'price'),)


class OrganizationAdmin(admin.ModelAdmin):
    """
    Админка предприятия
    """
    model = Organization
    list_display = ('name', 'concern')
    fields = (('name', 'concern'), ('description', 'districts'))
    search_fields = ('name', 'concern__name')
    ordering = ('concern', 'name')
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 10, 'cols': 30})},
    }
    inlines = [CatalogInlineAdmin]


class CatalogAdmin(admin.ModelAdmin):
    """
    Админка предприятия
    """
    form = CatalogForm
    list_display = ('organization', 'product', 'price', 'concern_product_price')
    list_filter = (('product', admin.RelatedFieldListFilter),)
    search_fields = ('product__name',)
    ordering = ('organization', 'product', 'price')

    def concern_product_price(self, instance):
        return instance.product.price

    concern_product_price.short_description = 'Цена сети предприятий'

    class Meta:
        model = Catalog


admin.site.register(Concern, ConcernAdmin)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Catalog, CatalogAdmin)
