from django.db import models
from django.core.validators import MinValueValidator
from django.core.exceptions import ValidationError
from city.models import District
from product.models import Product


class Concern(models.Model):
    """
    Модель "Сеть предприятий"
    """
    name = models.CharField(max_length=255, verbose_name='Название')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'сеть предприятий'
        verbose_name_plural = 'сети предприятий'


class Organization(models.Model):
    """
    Модель "Организация"
    """
    name = models.CharField(max_length=255, verbose_name='Название')
    description = models.TextField(verbose_name='Описание', blank=True, null=True)
    districts = models.ManyToManyField(District, verbose_name='Районы', related_name='organizations')
    concern = models.ForeignKey(Concern, verbose_name='Концерн', on_delete=models.PROTECT)

    def __str__(self):
        return '{0} ({1})'.format(self.name, self.concern.name)

    class Meta:
        verbose_name = 'предприятие'
        verbose_name_plural = 'предприятия'


class Catalog(models.Model):
    """
    Модель "Каталог (услуг/товара) организации"
    """
    organization = models.ForeignKey(Organization, verbose_name='Предприятие', on_delete=models.PROTECT,
                                     related_name='catalog_products')
    product = models.ForeignKey(Product, verbose_name='Услуга/Товар', on_delete=models.PROTECT)
    price = models.FloatField(verbose_name='Цена', validators=[MinValueValidator(0)])

    def __str__(self):
        return '{0} ({1}) - {2}'.format(self.product.name, self.organization.name, self.price)

    def clean(self):
        if self.product.concern != self.organization.concern:
            raise ValidationError({'product': 'Товар выпускается другим концерном!'})

    class Meta:
        verbose_name = 'товар'
        verbose_name_plural = 'список товаров/услуг'
