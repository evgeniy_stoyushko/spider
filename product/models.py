from django.db import models
from django.core.validators import MinValueValidator
from city.models import District


class Category(models.Model):
    """
    Модель "Категория (товара)"
    """
    name = models.CharField(max_length=255, verbose_name='Категория товара/услуги')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'категории'


class Product(models.Model):
    """
    Модель "Товар/Услуга"
    """
    name = models.CharField(max_length=255, verbose_name='Название')
    category = models.ForeignKey(Category, verbose_name='Категория', on_delete=models.PROTECT, related_name='products')
    concern = models.ForeignKey('organization.Concern', verbose_name='Сеть предприятий', related_name='products',
                                on_delete=models.PROTECT)
    price = models.FloatField(verbose_name='Цена в сети', validators=[MinValueValidator(0)])

    def __str__(self):
        return '{0} ({1}) - {2}'.format(self.name, self.concern.name, self.price)

    class Meta:
        verbose_name = 'услуга/товар'
        verbose_name_plural = 'услуги/товары'
