from rest_framework import viewsets
from .models import Category, Product
from .serializers import CategorySerializer, CategorySimpleSerializer, ProductSerializer, ProductSimpleSerializer


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ViewSet детального просмотра категорий товаров/услуг
    """
    queryset = Category.objects.all()
    serializer_class = CategorySimpleSerializer

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = CategorySerializer
        return super(CategoryViewSet, self).retrieve(request, *args, **kwargs)


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ViewSet детального просмотра товаров/услуг
    """
    queryset = Product.objects.all()
    serializer_class = ProductSimpleSerializer

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = ProductSerializer
        return super(ProductViewSet, self).retrieve(request, *args, **kwargs)

