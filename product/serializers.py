from rest_framework import serializers
from .models import Product, Category


class ProductSimpleSerializer(serializers.HyperlinkedModelSerializer):
    """
    Сериализатор для списка продуктов
    """
    class Meta:
        model = Product
        fields = ('id', 'url', 'name', 'price')


class ProductSerializer(serializers.ModelSerializer):
    """
    Сериализатор одного продукта
    """
    class Meta:
        model = Product
        fields = ('id', 'url', 'name', 'price', 'concern', 'category',)
        depth = 1


class CategorySimpleSerializer(serializers.HyperlinkedModelSerializer):
    """
    Сериализатор для списка категорий
    """
    class Meta:
        model = Category
        fields = ('id', 'url', 'name')


class CategorySerializer(serializers.ModelSerializer):
    """
    Сериализатор категории
    """
    products = ProductSimpleSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'url', 'name', 'products')
